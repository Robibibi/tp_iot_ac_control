# aws_dynamodb_table
resource "aws_dynamodb_table" "Temperature-db" {
  name           = "Temperature"
  billing_mode   = "PROVISIONED"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "id"

  attribute {
    name = "id"
    type = "S"
  }
}